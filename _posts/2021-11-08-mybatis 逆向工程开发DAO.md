---
layout:     post
title:      mybatis逆向工程开发DAO
subtitle:   逆向工程实际上就是mybatis的代理开发，只不过借助了插件来快速生成mapper接口和mapper.xml
date:       2021-11-08
author:     zhaoeh
header-img: img/post-bg-rwd.jpg
catalog: true
tags:
    - mybatis
---

# 1. 