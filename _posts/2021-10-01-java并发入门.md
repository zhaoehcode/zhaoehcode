---
layout:     post
title:      java并发必须知道的几个概念
subtitle:   先熟悉并发场景下的几个常见概念
date:       2021-10-01
author:     zhaoeh
header-img: img/post-bg-swift2.jpg
catalog: true
tags:
    - java高并发
---

# 1. 同步和异步

# 2. 并发和并行

# 3. 临界区

# 4. 阻塞（Blocking）和非阻塞（Non-Blocking）

# 5. 死锁（Deadlock）、饥饿（Starvation）和活锁（Livelock）
