---
layout:     post
title:      并发级别
subtitle:   并发存在对应的级别
date:       2021-10-02
author:     zhaoeh
header-img: img/post-bg-swift2.jpg
catalog: true
tags:
    - java高并发
---

# 1. 阻塞

# 2. 无饥饿(Starvation-Free)

# 3. 无障碍(Obstruction-Free)

# 4. 无锁(Lock-Free)

# 5. 无等待
